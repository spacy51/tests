#include <iostream>
#include <functional>


unsigned little_gauss(unsigned n)
{
	return (n * n + n) / 2;
}


unsigned sum_tail_recursive_internal(unsigned n, unsigned intermediate_sum)
{
	return (0 == n) ?
		intermediate_sum
		:
		sum_tail_recursive_internal(n - 1, n + intermediate_sum); // recursion is at the end of the function
}


unsigned sum_tail_recursive(unsigned n) // hide intermediate_sum parameter from caller
{
	return sum_tail_recursive_internal(n, 0);
}


unsigned sum_tail_recursive_lambda(unsigned n)
{
	// use recursive nested function
	std::function<unsigned(unsigned, unsigned)> sum_tail_recursive_internal =
		[&sum_tail_recursive_internal]
	(unsigned n, unsigned intermediate_sum)
	{
		return (0 == n) ?
			intermediate_sum
			:
			sum_tail_recursive_internal(n - 1, n + intermediate_sum); // recursion is at the end of the nested function
	};

	return sum_tail_recursive_internal(n, 0);
}


// tested with Visual Studio Express 2013 for Windows Desktop
int main()
{
	const unsigned N = 100000;

	// math for the win
	std::cout << little_gauss(N) << std::endl;

	// no stack overflow in Release build (optimized with /O2 compiler option)
	std::cout << sum_tail_recursive(N) << std::endl;

	// stack overflow occurs even in Release build (seems like Microsoft compiler does not optimize nested functions)
	std::cout << sum_tail_recursive_lambda(N) << std::endl;

	return 0;
}
