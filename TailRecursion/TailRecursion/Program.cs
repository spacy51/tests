﻿using System;

namespace TailRecursion
{
    class Program
    {
        public static uint little_gauss(uint n)
        {
            return (n * n + n) / 2;
        }

        public static uint sum_recursive(uint n)
        {
            return (0 == n) ?
                0
                :
                n + sum_recursive(n - 1);
        }

        public static uint sum_tail_recursive_internal(uint n, uint intermediate_sum)
        {
            return (0 == n) ?
                intermediate_sum
                :
                sum_tail_recursive_internal(n - 1, n + intermediate_sum);
        }

        public static uint sum_tail_recursive(uint n)
        {
            return sum_tail_recursive_internal(n, 0);
        }

        static void Main(string[] args)
        {
            const uint N = 42;

            // math for the win
            Console.WriteLine(little_gauss(N));

            // stack overflow exception for big N
            Console.WriteLine(sum_recursive(N));

            // stack overflow exception as well, because C#-compiler does not optimize it
            Console.WriteLine(sum_tail_recursive(N));
        }
    }
}
