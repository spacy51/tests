﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reactive.Linq;

//down   --a---------b-----------------
//move   -0-1-2-3-4-5-6-7-8-9----------
//up     -------a-----------b----------
//result --a1-2-a----b6-7-8-b----------


//result --a1-2-a----b6-7-8-b----------
//result -a1-2-a----b6-7-8-b----------

namespace FirstRxInWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var mouseDown = Observable.FromEventPattern<MouseButtonEventArgs>(this, "MouseDown")
                .Select(ea => ea.EventArgs.GetPosition(this));

            var mouseUp = Observable.FromEventPattern<MouseButtonEventArgs>(this, "MouseUp")
                .Select(ea => ea.EventArgs.GetPosition(this));

            var mouseMove = Observable.FromEventPattern<MouseEventArgs>(this, "MouseMove")
                .Select(ea => ea.EventArgs.GetPosition(this));

            // query comprehension syntax:
            //var position = from start in mouseDown
            //        from pos in mouseMove.StartWith(start).TakeUntil(mouseUp)
            //        select pos;
            var position = mouseDown.SelectMany(start => mouseMove.StartWith(start).TakeUntil(mouseUp));

            var delta = position.Zip(position.Skip(1),
                (prev, cur) => cur - prev);

            delta.ObserveOnDispatcher().Subscribe(value =>
                {
                    Canvas.SetLeft(image, Canvas.GetLeft(image) + value.X);
                    Canvas.SetTop(image, Canvas.GetTop(image) + value.Y);
                });
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            CaptureMouse();
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ReleaseMouseCapture();
        }
    }
}
